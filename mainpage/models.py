# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.

# python manage.py makemigrations mainpage
# python manage.py migrate mainpage

class userMessage(models.Model):  #
    userName= models.CharField(max_length=32)   #帐号
    passWord = models.CharField(max_length = 32)    #密码
    realName = models.CharField(max_length=32)   #姓名
    sex = models.CharField(max_length=5)       #性别
    userClass=models.CharField(max_length = 32)    #用户类别（管理员or读者）
    callMeth = models.EmailField(max_length = 32)    #邮箱--
    address= models.CharField(max_length = 50)  #地址

    
class bookMessage(models.Model):  #
    bookCode = models.IntegerField(primary_key=True)   #图书编号
    bookName = models.CharField(max_length = 50)    #书名
    bookClass=models.CharField(max_length = 32)    #图书类别
    whereIs = models.CharField(max_length = 32)    #典藏部门
    status = models.CharField(max_length=32)   #流通状态
    outTime = models.IntegerField(default = 30) #应还期限

    
class history(models.Model):
    hisId=models.IntegerField(primary_key=True )#id
    userName=models.CharField(max_length=32)   #借阅人
    bookCode = models.IntegerField(default = 0)   #图书编号
    bookName = models.CharField(max_length = 50)    #书名
    activeClass=models.CharField(max_length = 25)    #类型（借书or还书）
    activeTime = models.DateTimeField(auto_now =True) #更改日期
    
    
"""
电脑上运行、连接MySQL数据库（初始100本书）
1.管理员登录。管理员个人信息（编号、姓名、性别、联系方式、住址），管理图书信息。
2.(这个不需要了)馆藏图书检索。读者需要根据键入词及检索方式检索所需结果。检索结果呈现格式有书目详细信息（图书编号、书名、作者、内容简介、主题、出版社、）、馆内流通信息（索书号、登录号、典藏部门、应还期限）
3.个人信息查询。读者输入用户名及密码，登录个人中心。账号信息（用户编号、用户姓名、性别、住址、到期时间、用户类型）、借书信息（图书编号、典藏部门、流通状态、应还期限、续借）、借阅历史等
4.借书信息。显示读者所借阅图书的详细信息，书名、图书编号典藏部门、流通状态、应还日期及需要续借的功能，时长默认一个月。
借阅历史。显示读者借阅历史。包括书名、图书编号、类型（借书or还书）和更改日期
5.个性化图书推荐。利用读者借阅图书信息、检索图书信息，向读者推荐相似图书。
"""

